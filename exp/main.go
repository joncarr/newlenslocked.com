package main

import "fmt"

type Cat struct{}

func (c Cat) Speak() {
	fmt.Println("Meow!")
}

type Dog struct{}

func (d Dog) Speak() {
	fmt.Println("Woof!")
}

type Husky struct {
	Speaker
}

type Speaker interface {
	Speak()
}

func main() {
	h := Husky{Cat{}}

	h.Speak()

}
