package main

import (
	"flag"
	"fmt"
	"net/http"

	"newlenslocked.com/rand"

	"github.com/gorilla/csrf"

	"newlenslocked.com/middleware"
	"newlenslocked.com/models"

	_ "github.com/jinzhu/gorm/dialects/postgres"

	"newlenslocked.com/controllers"

	"github.com/gorilla/mux"
)

func main() {
	prodFlag := flag.Bool("prod", false, "Provide this flag in production. This ensures a .config file is provided before the application starts")
	flag.Parse()

	cfg := LoadConfig(*prodFlag)
	dbCfg := cfg.Database
	service, err := models.NewService(
		models.WithGorm(dbCfg.Dialect(), dbCfg.ConnectionString()),
		models.WithLogMode(!cfg.IsProd()),
		models.WithUser(cfg.Pepper, cfg.HMACKey),
		models.WithGallery(),
		models.WithImage(),
	)
	must(err)

	defer service.Close()
	service.AutoMigrate()

	r := mux.NewRouter()

	// Controllers
	staticC := controllers.NewStaticController()
	usersC := controllers.NewUsersController(service.User)
	galleriesC := controllers.NewGalleriesController(service.Gallery, service.Image, r)

	// Middleware
	csrfBytes, err := rand.GetBytes(32)
	must(err)
	csrfMw := csrf.Protect(csrfBytes, csrf.Secure(cfg.IsProd()))
	userMw := middleware.User{
		UserService: service.User,
	}

	requireUserMw := middleware.RequireUser{
		User: userMw,
	}

	// Routes
	r.Handle("/", staticC.HomeView).Methods("GET")
	r.Handle("/contact", staticC.ContactView).Methods("GET")
	r.HandleFunc("/signup", usersC.New).Methods("GET")
	r.HandleFunc("/signup", usersC.Create).Methods("POST")
	r.Handle("/login", usersC.LoginView).Methods("GET")
	r.HandleFunc("/login", usersC.Login).Methods("POST")
	r.HandleFunc("/logout", requireUserMw.ApplyFn(usersC.Logout)).Methods("POST")

	// Assets
	assetHandler := http.FileServer(http.Dir("./assets"))
	assetHandler = http.StripPrefix("/assets/", assetHandler)
	r.PathPrefix("/assets/").Handler(assetHandler)

	// Image
	imageHandler := http.FileServer(http.Dir("./images/"))
	r.PathPrefix("/images/").Handler(http.StripPrefix("/images/", imageHandler))

	// Gallery Routes
	r.Handle("/galleries", requireUserMw.ApplyFn(galleriesC.Index)).Methods("GET")
	r.Handle("/galleries/new", requireUserMw.Apply(galleriesC.New)).Methods("GET")
	r.HandleFunc("/galleries", requireUserMw.ApplyFn(galleriesC.Create)).Methods("POST")
	r.HandleFunc("/galleries/{id:[0-9]+}/edit", requireUserMw.ApplyFn(galleriesC.Edit)).Methods("GET").Name(controllers.EditGallery)
	r.HandleFunc("/galleries/{id:[0-9]+}/update", requireUserMw.ApplyFn(galleriesC.Update)).Methods("POST")
	r.HandleFunc("/galleries/{id:[0-9]+}/delete", requireUserMw.ApplyFn(galleriesC.Delete)).Methods("POST")
	r.HandleFunc("/galleries/{id:[0-9]+}/images", requireUserMw.ApplyFn(galleriesC.ImageUpload)).Methods("POST")
	r.HandleFunc("/galleries/{id:[0-9]+}/images/{filename}/delete", requireUserMw.ApplyFn(galleriesC.ImageDelete)).Methods("POST")
	r.HandleFunc("/galleries/{id:[0-9]+}", galleriesC.Show).Methods("GET").Name(controllers.ShowGallery)

	fmt.Printf("Server started at localhost:%d...\n", cfg.Port)
	http.ListenAndServe(fmt.Sprintf(":%d", cfg.Port), csrfMw(userMw.Apply(r)))

}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
