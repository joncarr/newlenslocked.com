package controllers

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"

	"newlenslocked.com/context"
	"newlenslocked.com/models"
	"newlenslocked.com/views"
)

const (
	ShowGallery = "show_gallery"
	EditGallery = "edit_gallery"

	maxMultipartMem = 1 << 20 // 1 megabyte
)

// NewGalleriesController instantiates a gallery controller and returns
// a pointer to a GalleriesController
func NewGalleriesController(gs models.GalleryService, is models.ImageService, r *mux.Router) *GalleriesController {
	return &GalleriesController{
		New:       views.NewView("bootstrap", "galleries/new"),
		IndexView: views.NewView("bootstrap", "galleries/index"),
		ShowView:  views.NewView("bootstrap", "galleries/show"),
		EditView:  views.NewView("bootstrap", "galleries/edit"),
		gs:        gs,
		is:        is,
		r:         r,
	}
}

// GalleriesController is the gallery controller struct
type GalleriesController struct {
	New       *views.View
	IndexView *views.View
	ShowView  *views.View
	EditView  *views.View
	gs        models.GalleryService
	is        models.ImageService
	r         *mux.Router
}

// GalleryForm is a container for the new gallery form
type GalleryForm struct {
	Title string `schema:"title"`
}

// Index shows all galleries the requesting user is the owner of.
//
// GET /galleries
func (gc *GalleriesController) Index(w http.ResponseWriter, r *http.Request) {
	user := context.User(r.Context())
	galleries, err := gc.gs.ByUserID(user.ID)
	if err != nil {
		log.Println(err)
		http.Error(w, "Something went wrong.", http.StatusInternalServerError)
		return
	}
	var vd views.Data
	vd.Yield = galleries
	gc.IndexView.Render(w, r, vd)
}

// Show will show the gallery with the path provided gallery id
//
// GET /galleries/:id
func (gc *GalleriesController) Show(w http.ResponseWriter, r *http.Request) {
	gallery, err := gc.galleryByID(w, r)
	if err != nil {
		return
	}
	var vd views.Data
	vd.Yield = gallery
	gc.ShowView.Render(w, r, vd)
}

// Edit renders the gallery edit view if the requesting user is the
// gallery owner
//
// GET /galleries/:id/edit
func (gc *GalleriesController) Edit(w http.ResponseWriter, r *http.Request) {
	gallery, err := gc.galleryByID(w, r)
	if err != nil {
		return
	}
	user := context.User(r.Context())
	if gallery.UserID != user.ID {
		http.Error(w, "Gallery not found", http.StatusNotFound)
		return
	}
	var vd views.Data
	vd.Yield = gallery
	gc.EditView.Render(w, r, vd)
}

// Update updates the gallery resource
//
// POST /galleries/:id/update
func (gc *GalleriesController) Update(w http.ResponseWriter, r *http.Request) {
	gallery, err := gc.galleryByID(w, r)
	if err != nil {
		return
	}
	user := context.User(r.Context())
	if gallery.UserID != user.ID {
		http.Error(w, "Gallery not found", http.StatusNotFound)
		return
	}
	var vd views.Data
	vd.Yield = gallery
	var form GalleryForm
	if err := parseForm(r, &form); err != nil {
		log.Println(err)
		vd.SetAlert(err)
		gc.EditView.Render(w, r, vd)
		return
	}

	gallery.Title = form.Title
	err = gc.gs.Update(gallery)
	if err != nil {
		vd.SetAlert(err)
		gc.EditView.Render(w, r, vd)
		return
	}
	vd.Alert = &views.Alert{
		Level:   views.AlertLevelSuccess,
		Message: "Gallery successfully updated",
	}
	gc.EditView.Render(w, r, vd)
}

// Create is used to process the form when a user tries
// to create a new gallery
//
// POST /galleries
func (gc *GalleriesController) Create(w http.ResponseWriter, r *http.Request) {
	var vd views.Data
	var form GalleryForm
	if err := parseForm(r, &form); err != nil {
		vd.SetAlert(err)
		gc.New.Render(w, r, vd)
		return
	}

	user := context.User(r.Context())
	gallery := models.Gallery{
		Title:  form.Title,
		UserID: user.ID,
	}
	if err := gc.gs.Create(&gallery); err != nil {
		vd.SetAlert(err)
		gc.New.Render(w, r, vd)
		return
	}
	url, err := gc.r.Get(EditGallery).URL("id", fmt.Sprintf("%v", gallery.ID))
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/galleries", http.StatusFound)
		return
	}
	http.Redirect(w, r, url.Path, http.StatusFound)
}

// POST /galleries/:id/images
func (gc *GalleriesController) ImageUpload(w http.ResponseWriter, r *http.Request) {
	gallery, err := gc.galleryByID(w, r)
	if err != nil {
		return
	}
	user := context.User(r.Context())
	if gallery.UserID != user.ID {
		http.Error(w, "Gallery not found", http.StatusNotFound)
		return
	}
	var vd views.Data
	vd.Yield = gallery

	// Parse a multipart form
	err = r.ParseMultipartForm(maxMultipartMem)
	if err != nil {
		vd.SetAlert(err)
		gc.EditView.Render(w, r, vd)
		return
	}

	// Get all files uplaoded from the image upload form
	files := r.MultipartForm.File["images"]
	for _, f := range files {
		file, err := f.Open()
		if err != nil {
			vd.SetAlert(err)
			gc.EditView.Render(w, r, vd)
			return
		}
		defer file.Close()

		err = gc.is.Create(gallery.ID, file, f.Filename)
		if err != nil {
			vd.SetAlert(err)
			gc.EditView.Render(w, r, vd)
			return
		}

	}

	url, err := gc.r.Get(EditGallery).URL("id", fmt.Sprintf("%v", gallery.ID))
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/galleries", http.StatusFound)
		return
	}

	http.Redirect(w, r, url.Path, http.StatusFound)
}

// POST /galleries/:id/images/:filename/delete
func (gc *GalleriesController) ImageDelete(w http.ResponseWriter, r *http.Request) {
	gallery, err := gc.galleryByID(w, r)
	if err != nil {
		return
	}
	user := context.User(r.Context())
	if gallery.UserID != user.ID {
		http.Error(w, "Gallery not found", http.StatusNotFound)
		return
	}
	filename := mux.Vars(r)["filename"]
	i := models.Image{
		Filename:  filename,
		GalleryID: gallery.ID,
	}
	err = gc.is.Delete(&i)
	if err != nil {
		var vd views.Data
		vd.Yield = gallery
		vd.SetAlert(err)
		gc.EditView.Render(w, r, vd)
		return
	}
	url, err := gc.r.Get(EditGallery).URL("id", fmt.Sprintf("%v", gallery.ID))
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/galleries", http.StatusFound)
		return
	}
	http.Redirect(w, r, url.Path, http.StatusFound)
}

// Delete deletes the gallery resource
//
// POST /galleries/:id/delete
func (gc *GalleriesController) Delete(w http.ResponseWriter, r *http.Request) {
	gallery, err := gc.galleryByID(w, r)
	if err != nil {
		return
	}
	user := context.User(r.Context())
	if gallery.UserID != user.ID {
		http.Error(w, "Gallery not found", http.StatusNotFound)
		return
	}
	var vd views.Data
	err = gc.gs.Delete(gallery.ID)
	if err != nil {
		vd.Yield = gallery
		vd.SetAlert(err)
		gc.EditView.Render(w, r, vd)
		return
	}
	http.Redirect(w, r, "/galleries", http.StatusFound)
}

func (gc *GalleriesController) galleryByID(w http.ResponseWriter, r *http.Request) (*models.Gallery, error) {
	vars := mux.Vars(r)
	idStr := vars["id"]
	id, err := strconv.Atoi(idStr)
	if err != nil {
		log.Println(err)
		http.Error(w, "Invalid Gallery ID", http.StatusNotFound)
		return nil, err
	}
	gallery, err := gc.gs.ByID(uint(id))
	if err != nil {
		switch err {
		case models.ErrNotFound:
			http.Error(w, "Gallery not found", http.StatusNotFound)
		default:
			log.Println(err)
			http.Error(w, "Whoops! Something went wrong.", http.StatusInternalServerError)
		}
		return nil, err
	}
	images, _ := gc.is.ByGalleryID(gallery.ID)
	gallery.Images = images
	return gallery, nil
}
