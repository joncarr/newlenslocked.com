package controllers

import (
	"net/http"
	"time"

	"newlenslocked.com/context"
	"newlenslocked.com/models"
	"newlenslocked.com/rand"

	"newlenslocked.com/views"
)

// SignupForm is a structure to hold signup form values
type SignupForm struct {
	Name     string `schema:"name"`
	Email    string `schema:"email"`
	Password string `schema:"password"`
}

// NewUsersController instantiates a user controller and returns
// a pointer to UsersController
func NewUsersController(us models.UserService) *UsersController {
	return &UsersController{
		NewView:   views.NewView("bootstrap", "users/new"),
		LoginView: views.NewView("bootstrap", "users/login"),
		us:        us,
	}
}

// UsersController is the user controller struct
type UsersController struct {
	NewView   *views.View
	LoginView *views.View
	us        models.UserService
}

// New is used to render a form for a new user account
//
// GET /signup
func (uc *UsersController) New(w http.ResponseWriter, r *http.Request) {
	uc.NewView.Render(w, r, nil)
}

// Create is used to process the form when a user tries
// to create a new user account
//
// POST /signup
func (uc *UsersController) Create(w http.ResponseWriter, r *http.Request) {
	var vd views.Data
	var form SignupForm
	if err := parseForm(r, &form); err != nil {
		vd.SetAlert(err)
		uc.NewView.Render(w, r, vd)
		return
	}

	user := models.User{
		Name:     form.Name,
		Email:    form.Email,
		Password: form.Password,
	}
	if err := uc.us.Create(&user); err != nil {
		vd.SetAlert(err)
		uc.NewView.Render(w, r, vd)
		return
	}

	err := uc.signIn(w, &user)
	if err != nil {
		http.Redirect(w, r, "/login", http.StatusFound)
		return
	}

	alert := views.Alert{
		Level:   views.AlertLevelSuccess,
		Message: "Welcome! Thanks for signing up!",
	}
	views.RedirectAlert(w, r, "/galleries", http.StatusFound, alert)
}

// LoginForm is a structure to hold login form values
type LoginForm struct {
	Email    string `schema:"email"`
	Password string `schema:"password"`
}

// Login is used to verify the provided email address and
// password and then log the user in if they are correct.
//
// POST /login
func (uc *UsersController) Login(w http.ResponseWriter, r *http.Request) {
	vd := views.Data{}
	form := LoginForm{}

	if err := parseForm(r, &form); err != nil {
		vd.SetAlert(err)
		uc.LoginView.Render(w, r, vd)
		return
	}

	user, err := uc.us.Authenticate(form.Email, form.Password)
	if err != nil {
		switch err {
		case models.ErrNotFound:
			vd.AlertError("Invalid email address")
		default:
			vd.SetAlert(err)
		}
		uc.LoginView.Render(w, r, vd)
		return
	}

	err = uc.signIn(w, user)
	if err != nil {
		vd.SetAlert(err)
		uc.LoginView.Render(w, r, vd)
		return
	}

	http.Redirect(w, r, "/galleries", http.StatusFound)

}

// Logout is used to delete a users session cookie (remember_token) and
// then will update the iuser resource with a new remember token.
//
// POST /logout
func (uc *UsersController) Logout(w http.ResponseWriter, r *http.Request) {
	cookie := http.Cookie{
		Name:     "remember_token",
		Value:    "",
		Expires:  time.Now(),
		HttpOnly: true,
	}
	http.SetCookie(w, &cookie)
	user := context.User(r.Context())
	token, _ := rand.RememberToken()
	user.Remember = token
	uc.us.Update(user)
	http.Redirect(w, r, "/", http.StatusFound)
}

// signIn is used to sign in the provided user after sign up or
// log in with cookies
func (uc *UsersController) signIn(w http.ResponseWriter, user *models.User) error {
	if user.Remember == "" {
		token,
			err := rand.RememberToken()
		if err != nil {
			return err
		}
		user.Remember = token
		err = uc.us.Update(user)
		if err != nil {
			return err

		}
	}

	cookie := http.Cookie{
		Name:     "remember_token",
		Value:    user.Remember,
		HttpOnly: true,
	}

	http.SetCookie(w, &cookie)

	return nil
}
