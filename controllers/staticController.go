package controllers

import (
	"newlenslocked.com/views"
)

// NewStaticController constructs a StaticController containing
// static views
func NewStaticController() *StaticController {
	return &StaticController{
		HomeView:    views.NewView("bootstrap", "static/home"),
		ContactView: views.NewView("bootstrap", "static/contact"),
	}
}

// StaticController is a controller for static views.
type StaticController struct {
	HomeView    *views.View
	ContactView *views.View
}
