#!/bin/bash

# Change to the directory with our code that we plan to work from (on local file system)
cd "$GOPATH/src/newlenslocked.com"

echo "==== Releasing the application ===="
echo "  Deleting the local binary if it exists (so it isn't uploaded)..."
rm newlenslocked.com
echo "  Done!"

echo "  Deleting existing code on the remote server..."
ssh root@lenslocked.jecarr.xyz "rm -rf /root/go/src/newlenslocked.com"
echo "  Code deleted successfully!"

echo "  Uploading code..."
rsync -avr --exclude ".git/*" --exclude "exp/*" --exclude "images/*" ./ root@lenslocked.jecarr.xyz:/root/go/src/newlenslocked.com
echo "  Code uploaded successfully!"

echo "  Go getting dependencies..."
ssh root@lenslocked.jecarr.xyz "export GOPATH=/root/go; /usr/local/go/bin/go get golang.org/x/crypto/bcrypt"
ssh root@lenslocked.jecarr.xyz "export GOPATH=/root/go; /usr/local/go/bin/go get github.com/gorilla/mux"
ssh root@lenslocked.jecarr.xyz "export GOPATH=/root/go; /usr/local/go/bin/go get github.com/gorilla/schema"
ssh root@lenslocked.jecarr.xyz "export GOPATH=/root/go; /usr/local/go/bin/go get github.com/gorilla/csrf"
ssh root@lenslocked.jecarr.xyz "export GOPATH=/root/go; /usr/local/go/bin/go get github.com/lib/pq"
ssh root@lenslocked.jecarr.xyz "export GOPATH=/root/go; /usr/local/go/bin/go get github.com/jinzhu/gorm"

echo "  Building the code on the remote server..."
ssh root@lenslocked.jecarr.xyz "export GOPATH=/root/go; cd /root/app; /usr/local/go/bin/go build -o server /root/go/src/newlenslocked.com/*.go"
echo "  Binary built!"

echo "   Moving assets..."
ssh root@lenslocked.jecarr.xyz "cd /root/app; cp -R /root/go/src/newlenslocked.com/assets ."
echo "  Assets moved successfully!"

echo "   Moving views..."
ssh root@lenslocked.jecarr.xyz "cd /root/app; cp -R /root/go/src/newlenslocked.com/views ."
echo "  Views moved successfully!"

echo "   Moving Caddyfile..."
ssh root@lenslocked.jecarr.xyz "cd /root/app; cp -R /root/go/src/newlenslocked.com/Caddyfile ."
echo "  Caddyfile moved successfully!"

echo "   Restarting the server..."
ssh root@lenslocked.jecarr.xyz "sudo service lenslocked restart"
echo "  Server restarted successfully"

echo "   Restarting Caddy service..."
ssh root@lenslocked.jecarr.xyz "sudo service caddy restart"
echo "  Caddy service restarted successfully"

echo "====Done releasing application===="