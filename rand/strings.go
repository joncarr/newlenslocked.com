package rand

import (
	"crypto/rand"
	"encoding/base64"
)

// RememberTokenBytes sets the amount of bytes to be used
// when generating RememberTokens
const RememberTokenBytes = 32

// GetBytes generates n random bytes or an error if
// there was one.
func GetBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}

	return b, nil
}

// NBytes returns the number of bytes used in the base64
// URL encoded string
func NBytes(base64String string) (int, error) {
	b, err := base64.URLEncoding.DecodeString(base64String)
	if err != nil {
		return -1, err
	}
	return len(b), err
}

// GenString will generate a byte slice of size nBytes and
// return a string that is the base64 URL encoded version
// of that byte slice.
func GenString(nBytes int) (string, error) {
	b, err := GetBytes(nBytes)
	if err != nil {
		return "", err
	}

	return base64.URLEncoding.EncodeToString(b), nil
}

// RememberToken is a helper function designed to generate
// remember tokens of a predetermined byte size
func RememberToken() (string, error) {
	return GenString(RememberTokenBytes)
}
