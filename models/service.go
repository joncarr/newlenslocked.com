package models

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type ServiceConfig func(*Service) error

func WithGorm(dialect, connectionInfo string) ServiceConfig {
	return func(s *Service) error {
		db, err := gorm.Open(dialect, connectionInfo)
		if err != nil {
			return err
		}
		s.db = db
		return nil
	}
}

func WithLogMode(mode bool) ServiceConfig {
	return func(s *Service) error {
		s.db.LogMode(mode)
		return nil
	}
}

func WithUser(pepper, hmacKey string) ServiceConfig {
	return func(s *Service) error {
		s.User = NewUserService(s.db, pepper, hmacKey)
		return nil
	}
}

func WithGallery() ServiceConfig {
	return func(s *Service) error {
		s.Gallery = NewGalleryService(s.db)
		return nil
	}
}

func WithImage() ServiceConfig {
	return func(s *Service) error {
		s.Image = NewImageService()
		return nil
	}
}

// NewService constructs a new Service container.
// It accepts the database connection string and returns
// a pointer to a new Service and an error
func NewService(cfgs ...ServiceConfig) (*Service, error) {
	var s Service
	for _, cfg := range cfgs {
		if err := cfg(&s); err != nil {
			return nil, err
		}
	}
	return &s, nil
}

// Service is a container for all our different model services
type Service struct {
	Gallery GalleryService
	User    UserService
	Image   ImageService
	db      *gorm.DB
}

// Close closes the database connection
func (s *Service) Close() error {
	return s.db.Close()
}

// DestructiveReset drops all tables and rebuilds it
func (s *Service) DestructiveReset() error {
	err := s.db.DropTableIfExists(
		&User{},
		&Gallery{},
	).Error
	if err != nil {
		return err
	}

	return s.AutoMigrate()
}

// AutoMigrate will attempt to automatically migrate
// all tables
func (s *Service) AutoMigrate() error {
	return s.db.AutoMigrate(
		&User{},
		&Gallery{},
	).Error
}
