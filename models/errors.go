package models

import (
	"strings"
)

const (
	// ErrNotFound is returned when a resource can not be
	// found in the database.
	ErrNotFound modelError = "models: resource not found"

	// ErrPasswordIncorrect is returned when an invalid password is
	// provided to a method like Authenticate
	ErrPasswordIncorrect modelError = "models: incorrect password provided"

	// ErrEmailRequired is returned when an email address is not
	// provided when creating a user.
	ErrEmailRequired modelError = "models: email address is required"

	// ErrEmailInvalid is returned when a provided email address
	// does not match an of our requirements
	ErrEmailInvalid modelError = "models: email address is not valid"

	// ErrEmailAddressTaken is returned when an update or create is attempted
	// with an email address that is already in use
	ErrEmailAddressTaken modelError = "models: email address is already taken"

	// ErrPasswordTooShort is returned when an update or create is attempted
	// when a password is less than 8 characters
	ErrPasswordTooShort modelError = "models: password must be at least 8 characters long"

	// ErrRememberRequired is returned  when a create or update is attempted
	// without a remember token hash
	ErrRememberRequired modelError = "models: remember token is required"

	// ErrTitleRequired is returned when a gallery is attempted to be
	// created without a title
	ErrTitleRequired modelError = "models: title is required"

	// ErrPasswordRequired is returned when a create is attempted without a
	// user password provided
	ErrPasswordRequired privateError = "models: password is required"

	// ErrRememberTooShort is returned when a remember token is not at least // the specified number of bytes (Typically 32)
	ErrRememberTooShort privateError = "models: remember token must be at least 32 bytes"

	// ErrIDInvalid is returned when an invalid ID is provided
	// to a method like Delete
	ErrIDInvalid privateError = "models: ID provided is invalid"

	// ErrUserIDRequired is returned when a resource is attempted to be
	// submitted without an associated User ID
	ErrUserIDRequired privateError = "models: user ID is required"
)

type modelError string

func (e modelError) Error() string {
	return string(e)
}

func (e modelError) Public() string {
	s := strings.Replace(string(e), "models: ", "", 1)
	split := strings.Split(s, " ")
	split[0] = strings.Title(split[0])
	return strings.Join(split, " ")
}

type privateError string

func (e privateError) Error() string {
	return string(e)
}
